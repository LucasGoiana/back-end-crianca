FROM openjdk:11-jre
EXPOSE 8082

RUN mkdir /app
COPY target/*.jar /app/backend-crianca.jar
WORKDIR /app
#CMD "tail" "-f" "/dev/null"
CMD "java" "-jar" "/app/backend-crianca.jar"
