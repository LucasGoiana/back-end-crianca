#!/bin/bash

docker login -u $1 -p $2 registry.gitlab.com/lucasgoiana/back-end-crianca
docker pull registry.gitlab.com/lucasgoiana/back-end-crianca

countContainers=$(docker ps -a | wc -l)

echo $countContainers

if [ $countContainers -gt 1 ]
then
    docker rm -f $(docker ps -a -q)
fi

docker run -d -i --name back-end-crianca -p 8082:8082 -e STRING_CONNECTION=$3 -e USER_BD=$4 -e USER_PASS=$5  registry.gitlab.com/lucasgoiana/back-end-crianca
