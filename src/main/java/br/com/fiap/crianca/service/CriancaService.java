package br.com.fiap.crianca.service;

import br.com.fiap.crianca.dtos.CreateOrUpdateDTO;
import br.com.fiap.crianca.dtos.CriancaDTO;

import java.util.List;

public interface CriancaService {


    public List<CriancaDTO> getTodosFilhosPorPai(Integer id);

    CreateOrUpdateDTO cadastrarCrianca(CreateOrUpdateDTO CreateOrUpdateDTO);
    CreateOrUpdateDTO editarCrianca(Integer id, CreateOrUpdateDTO CreateOrUpdateDTO);

    List<CriancaDTO> getCriancaPorId(Integer id);

    CriancaDTO cadastrarCriancaPai(Integer id, Integer id2);
}
