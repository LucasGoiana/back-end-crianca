package br.com.fiap.crianca.service;

import br.com.fiap.crianca.dtos.CreateOrUpdateDTO;
import br.com.fiap.crianca.dtos.CriancaDTO;
import br.com.fiap.crianca.entities.CriancaEntity;
import br.com.fiap.crianca.entities.CriancaPaiEntity;
import br.com.fiap.crianca.repositories.CriancaPaiRepository;
import br.com.fiap.crianca.repositories.CriancaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class CriancaServiceImpl implements CriancaService {

    private final CriancaRepository criancaRepository;
    private final CriancaPaiRepository criancaPaiRepository;

    public CriancaServiceImpl(CriancaRepository criancaRepository, CriancaPaiRepository criancaPaiRepository) {
        this.criancaRepository = criancaRepository;
        this.criancaPaiRepository = criancaPaiRepository;
    }


    @Override
    public List<CriancaDTO> getTodosFilhosPorPai(Integer id)
    {
       var criancaList = criancaPaiRepository.findAllAtivas(id.toString());
       return criancaList.stream().map(CriancaDTO::new).collect(Collectors.toList());
    }

    @Override
    public CreateOrUpdateDTO cadastrarCrianca(CreateOrUpdateDTO createOrUpdateDTO) {
        CriancaEntity criancaEntity = new CriancaEntity(createOrUpdateDTO);
        criancaEntity.setNome(createOrUpdateDTO.getNome());
        criancaEntity.setCpf(createOrUpdateDTO.getCpf());
        criancaEntity.setIdade(createOrUpdateDTO.getIdade());
        criancaRepository.save(criancaEntity);

        createOrUpdateDTO.setId(criancaEntity.getId());

        CriancaPaiEntity criancaPaiEntity = new CriancaPaiEntity();
        criancaPaiEntity.setCriancaEntity(criancaEntity);
        criancaPaiEntity.setPai_id(createOrUpdateDTO.getPai_id());
        criancaPaiRepository.save(criancaPaiEntity);
        return createOrUpdateDTO;
    }

    @Override
    public List<CriancaDTO> getCriancaPorId(Integer id) {
        var criancaList =  criancaRepository.findById(id);;
        return criancaList.stream().map(CriancaDTO::new).collect(Collectors.toList());

    }

    @Override
    public CriancaDTO cadastrarCriancaPai(Integer id, Integer id2) {
        var crianca = criancaRepository.findById(id);

        CriancaDTO criancaDTO = new CriancaDTO(crianca.get());
        criancaDTO.setCrianca_id(id);
        criancaDTO.setPai_id(id2);
        CriancaPaiEntity criancaPaiEntity = new CriancaPaiEntity();
        criancaPaiEntity.setCriancaEntity(crianca.get());
        criancaPaiEntity.setPai_id(id2);
        criancaPaiRepository.save(criancaPaiEntity);
        return criancaDTO;
    }

    @Override
    public CreateOrUpdateDTO editarCrianca(Integer id, CreateOrUpdateDTO createOrUpdateDTO){
        var crianca = criancaRepository.findById(id);


        CriancaEntity criancaEntity = new CriancaEntity(createOrUpdateDTO);
        criancaEntity.setId(id);
        criancaEntity.setNome(createOrUpdateDTO.getNome());
        criancaEntity.setCpf(createOrUpdateDTO.getCpf());
        criancaEntity.setIdade(createOrUpdateDTO.getIdade());
        criancaEntity = criancaRepository.save(criancaEntity);
        createOrUpdateDTO.setId(id);
        return createOrUpdateDTO;
    }

}
