package br.com.fiap.crianca.controllers;

import br.com.fiap.crianca.dtos.CreateOrUpdateDTO;
import br.com.fiap.crianca.dtos.CriancaDTO;
import br.com.fiap.crianca.service.CriancaService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("crianca")
public class CriancaController {
    
    private final CriancaService criancaService;

    public CriancaController (CriancaService criancaService){
        this.criancaService = criancaService;
    }

    @GetMapping("/ping")
    public String test(){
        return "Pong";
    }

    @GetMapping("/{id}/pai/")
    public List<CriancaDTO> getTodosFilhosPorPai(@PathVariable Integer id){
        return criancaService.getTodosFilhosPorPai(id);
    }

    @GetMapping("/{id}/")
    public List<CriancaDTO> getCriancaPorId(@PathVariable Integer id){
        return criancaService.getCriancaPorId(id);
    }

    @PostMapping("/")
    public CreateOrUpdateDTO cadastrarCrianca(@RequestBody CreateOrUpdateDTO createOrUpdateDTO){
        return criancaService.cadastrarCrianca(createOrUpdateDTO);
    }

    @GetMapping("/{id}/crianca/{id2}/pai")
    public CriancaDTO cadastrarCriancaPai(@PathVariable Integer id, @PathVariable Integer id2){
        return criancaService.cadastrarCriancaPai(id, id2);
    }

    @PutMapping("/{id}/")
    public CreateOrUpdateDTO editarCrianca(@PathVariable Integer id, @RequestBody CreateOrUpdateDTO createOrUpdateDTO){
        return criancaService.editarCrianca(id, createOrUpdateDTO);
    }

}
