package br.com.fiap.crianca.dtos;

import br.com.fiap.crianca.entities.CriancaEntity;
import br.com.fiap.crianca.entities.CriancaPaiEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CriancaDTO {

    private String nome;
    private Integer idade;
    private Integer crianca_id;
    private Integer pai_id;
    private String cpf;

    public CriancaDTO(CriancaPaiEntity criancaPaiEntity){
        this.nome = criancaPaiEntity.getCriancaEntity().getNome();
        this.idade = criancaPaiEntity.getCriancaEntity().getIdade();
        this.cpf = criancaPaiEntity.getCriancaEntity().getCpf();
        this.crianca_id = criancaPaiEntity.getCriancaEntity().getId();
        this.pai_id = criancaPaiEntity.getPai_id();

    }

    public CriancaDTO(CriancaEntity criancaEntity){
        this.nome = criancaEntity.getNome();
        this.idade = criancaEntity.getIdade();
        this.cpf = criancaEntity.getCpf();

    }
}
