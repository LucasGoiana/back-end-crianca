package br.com.fiap.crianca.dtos;

import br.com.fiap.crianca.entities.CriancaEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateOrUpdateDTO {
    private Integer id;
    private Integer pai_id;
    private String nome;
    private Integer idade;
    private String cpf;


}
