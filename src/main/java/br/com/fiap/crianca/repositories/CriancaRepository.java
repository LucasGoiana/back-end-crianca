package br.com.fiap.crianca.repositories;

import br.com.fiap.crianca.dtos.CriancaDTO;
import br.com.fiap.crianca.entities.CriancaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CriancaRepository extends JpaRepository<CriancaEntity, Integer> {

}
