package br.com.fiap.crianca.repositories;

import br.com.fiap.crianca.dtos.CriancaDTO;
import br.com.fiap.crianca.entities.CriancaEntity;
import br.com.fiap.crianca.entities.CriancaPaiEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CriancaPaiRepository extends JpaRepository<CriancaPaiEntity, Integer> {

    @Query(value = "select f.id, f.nome, f.idade, f.cpf, cp.crianca_id, cp.pai_id from filho f inner join crianca_pai cp on f.id = cp.crianca_id where pai_id = :pai_id group by f.id",  nativeQuery = true)
    List<CriancaPaiEntity> findAllAtivas(@Param("pai_id") String pai_id);
}
