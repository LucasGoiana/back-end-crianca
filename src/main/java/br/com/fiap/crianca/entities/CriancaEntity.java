package br.com.fiap.crianca.entities;

import br.com.fiap.crianca.dtos.CreateOrUpdateDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@AllArgsConstructor
@Table(name="filho")
public class CriancaEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id", unique = true)
    private Integer id;
    private String nome;
    private Integer idade;
    private String cpf;

    public CriancaEntity(CreateOrUpdateDTO createOrUpdateDTO){
        this.id = createOrUpdateDTO.getId();
        this.nome = createOrUpdateDTO.getNome();
        this.idade = createOrUpdateDTO.getIdade();
        this.cpf = createOrUpdateDTO.getCpf();
    }

    public CriancaEntity(String nome, int idade, String cpf) {
        this.nome = nome;
        this.idade = idade;
        this.cpf = cpf;
    }

    public CriancaEntity(int idade, String cpf) {
        this.idade = idade;
        this.cpf = cpf;
    }


}
