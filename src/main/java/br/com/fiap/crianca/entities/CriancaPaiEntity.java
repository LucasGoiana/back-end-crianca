package br.com.fiap.crianca.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@AllArgsConstructor
@Table(name="crianca_pai")
public class CriancaPaiEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id", unique = true)
    private Integer id;

    @ManyToOne(optional = true)
    @JoinColumn(name = "crianca_id")
    private CriancaEntity criancaEntity;
    private Integer pai_id;

}
