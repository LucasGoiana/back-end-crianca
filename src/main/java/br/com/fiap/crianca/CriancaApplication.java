package br.com.fiap.crianca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CriancaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CriancaApplication.class, args);
	}

}
