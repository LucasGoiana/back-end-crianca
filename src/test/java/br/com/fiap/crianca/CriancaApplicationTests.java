package br.com.fiap.crianca;

import br.com.fiap.crianca.entities.CriancaEntity;
import br.com.fiap.crianca.repositories.CriancaRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static java.nio.file.Paths.get;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@DataJpaTest
class CriancaApplicationTests {

	@Autowired
	private CriancaRepository criancaRepository;


	@Test
	public void insertUser() {
		CriancaEntity user = new CriancaEntity("name", 11, "DOC12355");
		criancaRepository.save(user);
		Integer countUser = criancaRepository.findAll().size();
		assertEquals(1, countUser);
	}

	@Test
	public void insertUserNoteName() {
		CriancaEntity user = new CriancaEntity(11, "DOC12355");
		criancaRepository.save(user);
		Integer countUser = criancaRepository.findAll().size();
		assertEquals(1, countUser);
	}

	@Test
	public void checkFindById() {
		CriancaEntity criancaEntity = new CriancaEntity("name", 11, "DOC12355");;
		criancaRepository.save(criancaEntity);
		Integer countUser = criancaRepository.findAll().size();
		assertEquals(1, countUser);
		Optional<CriancaEntity> criancaEntity1 = criancaRepository.findById(criancaEntity.getId());

		assertNotNull(criancaEntity);
		assertEquals(criancaEntity, criancaEntity);
	}

	@Test
	public void checkChilds() {
		var xpto = criancaRepository.findAll();
		assertNotNull(xpto, "Existem crianças cadastradas");
	}

}
